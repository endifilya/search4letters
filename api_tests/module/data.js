const got = require('got')
const {CookieJar} = require('tough-cookie');
const {promisify} = require('util');

class GenerateData {
    

async getSession() {
    const response = await got('http://172.17.0.1:5000/login')
    const auth = JSON.parse(JSON.stringify(response.headers))
    let re = /(?<==)([\s\S]+?)(?=;)/
    let token = auth['set-cookie'][0].split(re)

    return token[0]+token[1]
}

async setCookie(map) {
        let cookieJar = new CookieJar();
        const setCookie = promisify(cookieJar.setCookie.bind(cookieJar));

        for (let entry of map) {
            await setCookie( entry[0], entry[1]);
        }

        return cookieJar
    }

}
module.exports = new GenerateData;