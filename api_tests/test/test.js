const got = require('got')
const assert = require('assert')
const FormData = require('form-data');
const genData = require('../module/data.js')
const config = require('../config/conf.js')


describe('Действия доступные пользователю', function() {
    this.timeout(20000)

    it('Проверка авторизации /login', async function() {
        const response = await got(`http://${config.host}:${config.port}/login`)
        const result = JSON.parse(JSON.stringify(response.body))
        assert(result == 'Nice, you are logged in')
    })

   /* it('Проверка запроса /search4 на поиск символов "a" в строке', async function() {
        const form = new FormData();
        form.append('phrase', 'dasdad');
        form.append('letters', 'aeiou');

        const response = await got.post(`http://${config.host}:${config.port}/search4`, {
            headers: { 'User-Agent': 'Mozilla/5.0 (platform; rv:geckoversion) Gecko/geckotrail Firefox/firefoxversion'},
            body: form
        })
        const result = JSON.parse(JSON.stringify(response.body))
        //assert(result == {'a'}, `should return status 200, but status = ${status}`)
    })
*/
    it('Проверка просмотра логов не авторизированным', async function() {
        const response = await got(`http://${config.host}:${config.port}/logs`)
        const result = JSON.parse(JSON.stringify(response.body))
        assert(result == 'You are not logged in')
    })

    it('Проверка просмотра логов авторизированным', async function() {
        const session = await genData.getSession();
        let data = new Map([
            [session, 'localhost'],
          ]);
        
        const cookies = await genData.setCookie(data);
        const response = await got(`http://${config.host}:${config.port}/logs`, {
            cookies,
            headers : {Cookie: session}
        })

        const result = JSON.parse(JSON.stringify(response.body))
        assert(result !== 'You are not logged in')
    })


    
})