FROM rust:1.45.0-alpine

WORKDIR /app
COPY . ./app

RUN apk add --update --no-cache python3 && ln -sf python3 /usr/bin/python
RUN apk add bash py-pip build-base python3-dev libffi-dev musl-dev openssl-dev cargo &&\
    pip3 install -r app/requirements.txt 

EXPOSE 5000
    




